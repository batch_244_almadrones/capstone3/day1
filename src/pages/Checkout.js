import { useContext, useState, useEffect } from "react";
import { Col, Row, Card, Form } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import "../css/checkout.css";
import UserContext from "../UserContext";
import { AiOutlineHome } from "react-icons/ai";
import { IoCashOutline, IoWalletOutline } from "react-icons/io5";
import Swal from "sweetalert2";
export default function Checkout() {
  const { user } = useContext(UserContext);
  const location = useLocation();
  const product = location.state?.product;
  const [modeOfPayment, setModeOfPayment] = useState(null);
  const [isDisabled, setIsActive] = useState(true);
  const newProductDetails = [];
  let subtotal = 0;
  const shippingFee = 100 * product.length;

  // useEffect(() => {
  //   setCod(!wallet);
  // }, [wallet]);

  // useEffect(() => {
  //   setWallet(!cod);
  // }, [cod]);

  //console.log(product);
  useEffect(() => {
    if (modeOfPayment !== null && isDisabled) {
      setIsActive(false);
    }
  }, [modeOfPayment]);

  function calcSubtotal(num) {
    subtotal += num;
  }

  function placeOrderNow() {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkOutOrder`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(newProductDetails),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Order placed!",
            icon: "success",
            text: "Your order is on its way!",
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  }

  return (
    <div className="d-md-flex container">
      <Col className="col-xs-1 col-md-6 container">
        <Row>
          <div>
            <span>
              {user.firstName} {user.lastName}
            </span>
            <span>{user.mobileNo}</span>
            <p>Delivery Address</p>
            <div>
              <span>
                <AiOutlineHome />
              </span>
              <span>{user.address}</span>
            </div>
          </div>
        </Row>
        <Row>
          {product.map((item, index) => (
            <div className="d-flex" key={index}>
              <p hidden>
                {calcSubtotal(item.price * item.quantity)}
                {(item.subtotal = item.price * item.quantity)}
                {newProductDetails.push({
                  productId: item._id,
                  productName: item.name,
                  quantity: item.quantity,
                  modeOfPayment: modeOfPayment,
                  subtotal: item.subtotal,
                })}
                {console.log(newProductDetails)}
              </p>
              <img
                className="img-fluid checkout-product-image"
                src={item.image.url}
              />
              <div>
                <p>{item.name}</p>
                <p>{item.description}</p>
              </div>
              <div>
                <p>{item.price}</p>
                <p>{item.quantity}</p>
              </div>
            </div>
          ))}
        </Row>
      </Col>

      <Col className="col-xs-1 col-md-4">
        <Row>
          <div>
            <h6>Choose Payment Method</h6>
            <Card>
              <Card.Body className="position-relative">
                <Form.Check
                  inline
                  type="radio"
                  name="payment"
                  onClick={() => setModeOfPayment("COD")}
                  className="position-absolute end-0"
                  id="inline-radio-1"
                />
                <Card.Text>
                  <IoCashOutline />
                  Cash on Delivery
                </Card.Text>
              </Card.Body>
              <Card.Footer>Pay when delivered</Card.Footer>
            </Card>
            <Card className="mt-2">
              <Card.Body className="position-relative">
                <Form.Check
                  type="radio"
                  inline
                  name="payment"
                  onClick={() => setModeOfPayment("eWallet")}
                  className="position-absolute end-0"
                  id="inline-radio-2"
                />
                <Card.Text>
                  <IoWalletOutline />
                  Kbuild Pay
                </Card.Text>
              </Card.Body>
              <Card.Footer>Kbuild e-Wallet</Card.Footer>
            </Card>
            <div>
              <h6 className="mt-5">Order Summary</h6>

              <div className="position-relative">
                <span>Subtotal: </span>
                <span className="position-absolute end-0">
                  Php{" "}
                  {subtotal.toLocaleString("en", { minimumFractionDigits: 2 })}
                </span>
              </div>

              <div className="position-relative">
                <span>Shipping fee: </span>
                <span className="position-absolute end-0">
                  Php{" "}
                  {shippingFee.toLocaleString("en", {
                    minimumFractionDigits: 2,
                  })}
                </span>
              </div>

              <hr />
              <div className="position-relative">
                <span>Total: </span>
                <span className="position-absolute end-0">
                  Php{" "}
                  {(shippingFee + subtotal).toLocaleString("en", {
                    minimumFractionDigits: 2,
                  })}
                </span>
              </div>
            </div>
            <button
              className="checkout-btn-submit"
              disabled={isDisabled}
              onClick={() => placeOrderNow()}
            >
              Place Order Now
            </button>
          </div>
        </Row>
      </Col>
    </div>
  );
}
