import { useState, useContext, useRef, useEffect } from "react";
import { Card, Container, Button, Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/dashboard.css";
import AddProduct from "../components/AddProduct";

import {
  AiFillCloseCircle,
  AiOutlineUpload,
  AiOutlineFileImage,
} from "react-icons/ai";
// const fetchFromServer = require("../data/allProducts");
const AllProducts = () => {
  const [allProducts, setAllProducts] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [needToFetchData, isNeedToFetchData] = useState(0);
  const [initialData, setInitialData] = useState({
    name: "",
    description: "",
    price: "",
    supply: "",
    category: [],
    image: {
      public_id: "",
      url: "",
    },
  });

  const archiveProduct = (id, name) => {
    //console.log(id);
    fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Archive Successful",
            icon: "success",
            text: `${name} is now inactive.`,
          });
          getProduct();
        } else {
          Swal.fire({
            title: "Archive unsuccessful",
            icon: "error",
            text: "Something went wrong. Please try again later!",
          });
        }
      });
  };

  const activateProduct = (id, name) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activateProduct/${id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Successful",
            icon: "success",
            text: `${name} is now active.`,
          });
          // To show the update with the specific operation intiated.
          getProduct();
        } else {
          Swal.fire({
            title: "Archive unsuccessful",
            icon: "error",
            text: "Something went wrong. Please try again later!",
          });
        }
      });
  };

  const editProduct = (prod) => {
    setOpenModal(true);
    setIsEdit(true);
    setInitialData(prod);
    return (
      <AddProduct
        product={initialData}
        open={openModal}
        isEdit={isEdit}
        onClose={() => {
          setIsEdit(false);
          setOpenModal(false);
        }}
      />
    );
  };

  useEffect(() => {
    getProduct();
  }, [needToFetchData]);

  const getProduct = () => {
    // get all the courses from the database
    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        //console.log(data);
        setAllProducts(
          data.map((product) => {
            return (
              <tr key={product._id}>
                <td className="d-none">{product._id}</td>
                <td>
                  {
                    <img
                      src={product.image.url}
                      className="dashboard-product-img"
                    />
                  }
                </td>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.category}</td>
                <td>{product.price.toLocaleString("en")}</td>
                <td>{product.supply.toLocaleString("en")}</td>
                <td>{product.isActive ? "Active" : "Inactive"}</td>
                <td>
                  {product.isActive ? (
                    <Button
                      variant="danger"
                      size="sm"
                      onClick={() => archiveProduct(product._id, product.name)}
                    >
                      Archive
                    </Button>
                  ) : (
                    <>
                      <Button
                        variant="success"
                        size="sm"
                        className="mx-1"
                        onClick={() =>
                          activateProduct(product._id, product.name)
                        }
                      >
                        Unarchive
                      </Button>
                    </>
                  )}
                </td>
                <td>
                  {
                    <Button
                      variant="secondary"
                      size="sm"
                      className="mx-1"
                      onClick={() => {
                        editProduct(product);
                      }}
                    >
                      Edit
                    </Button>
                  }
                </td>
              </tr>
            );
          })
        );
      });
  };

  return (
    <Container className="manageProduct-body">
      <Button onClick={() => setOpenModal(true)} className="addProduct-openBtn">
        Add Product
      </Button>
      <AddProduct
        product={initialData}
        open={openModal}
        isEdit={isEdit}
        onClose={() => setOpenModal(false)}
      />

      <Table striped bordered hover className="mt-5" responsive="sm">
        <thead>
          <tr>
            <th className="d-none">Product ID</th>
            <th>Image</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Price</th>
            <th>Supply</th>
            <th>Status</th>
            <th>Archive</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
    </Container>
  );
};

const Inventory = () => {
  return (
    <>
      <p>"Inventory"</p>
    </>
  );
};

const Tabs = ({ config }) => {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className="dashboard-reg-tab">
      <div className="dashboard-reg-tab-headers">
        {config.map((entry, index) => (
          <div
            className={`dashboard-reg-tab-header  ${
              activeTab === index ? "active" : ""
            } `}
            onClick={() => setActiveTab(index)}
            key={index}
          >
            {entry.header}
          </div>
        ))}
      </div>
      <div className="dashboard-reg-tab-body">
        {config[activeTab].component}
      </div>
    </div>
  );
};

export default function LoginRegister() {
  return (
    <Container>
      <Tabs
        config={[
          // {
          //   header: "Add Product",
          //   component: <AddProduct />,
          // },
          { header: "Manage Products", component: <AllProducts /> },
          { header: "Product Inventory", component: <Inventory /> },
        ]}
      />
    </Container>
  );
}
