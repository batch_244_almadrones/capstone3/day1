import { useState, useContext, useRef, useEffect } from "react";
import { Form, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/loginRegister.css";
import {
  AiOutlineEyeInvisible,
  AiOutlineEye,
  AiFillFacebook,
  AiFillGoogleSquare,
  AiFillLinkedin,
} from "react-icons/ai";

import UserContext from "../UserContext";

const Login = () => {
  // Allows us to consume the User context object and its properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const emailRef = useRef();
  const passwordRef = useRef();

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  const [seePasswordState, setSeePwState] = useState(false);

  const toggleEye = () => {
    setSeePwState((prevState) => !prevState);
  };

  // useEffect(() => {
  //   console.log(user);
  //   console.log(localStorage);
  // }, [user]);

  function authenticate(e) {
    e.preventDefault();

    let email = emailRef.current.value;
    let password = passwordRef.current.value;
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        if (data.access !== undefined) {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);
          // retrieveUserDetails();

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            showConfirmButton: false,
            backdrop: `rgba(24, 116, 152,0.4)`,
            timer: 1500,
          });
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again!",
          });
        }
      });
    email = "";
    password = "";
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // Changes the global user state tos tore the "id" and the "idAdmin" property of the user which will be used for validation across the whole application
        setUser(data);
      });
  };

  const handleChange = () => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (emailRef.current.value !== "" && passwordRef.current.value !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  };

  return user._id != undefined ? (
    <Navigate to="/" />
  ) : (
    <Form className="text-center" onSubmit={(e) => authenticate(e)}>
      <Form.Group controlId="loginGroup" className="p-4 login-form">
        <div className="position-relative mb-5 login-soc-med">
          <div className="position-absolute start-0">
            <AiFillFacebook />
            <AiFillGoogleSquare />
            <AiFillLinkedin />
          </div>
        </div>
        <br />
        <Form.Control
          type="email"
          placeholder="Email"
          required
          ref={emailRef}
          className="mt-2"
          onChange={handleChange}
        />
        <br />
        <div className="login-password-container">
          <Form.Control
            type={seePasswordState ? "text" : "password"}
            placeholder="Password"
            ref={passwordRef}
            required
            onChange={handleChange}
          />
          <div className="password-eye" onClick={toggleEye}>
            {seePasswordState ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
          </div>
        </div>
        <br />
        <div className="d-flex position-relative mt-4">
          <Form.Check
            type="checkbox"
            label="Remember me"
            className="float-left"
          />
          <a className="position-absolute end-0" href="#">
            Forgot Password?
          </a>
        </div>

        <button className="login-btn-submit" disabled={!isActive}>
          Log in
        </button>
      </Form.Group>
    </Form>
  );
};

const Register = () => {
  const firstNameRef = useRef();
  const lastNameRef = useRef();
  const mobileNoRef = useRef();
  const emailRef = useRef();
  const password1Ref = useRef();
  const password2Ref = useRef();
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  function refreshPage() {
    return <Navigate to="/login" />;
  }

  const handleChange = () => {
    if (
      firstNameRef.current.value !== "" &&
      lastNameRef.current.value !== "" &&
      mobileNoRef.current.value !== "" &&
      emailRef.current.value !== "" &&
      password1Ref.current.value !== "" &&
      password1Ref.current.value == password2Ref.current.value
    ) {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  };

  function registerUser(e) {
    e.preventDefault();
    let firstName = firstNameRef.current.value;
    let lastName = lastNameRef.current.value;
    let mobileNo = mobileNoRef.current.value;
    let email = emailRef.current.value;
    let password = password1Ref.current.value;

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Registration Successful!",
            icon: "success",
            text: "Welcome to Zuitt!",
          }).then(() => {
            refreshPage();
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  }

  return (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="registerGroup" className="login-form">
        <br />
        <h5 className="pb-3">Register An Account</h5>
        <Form.Control
          type="text"
          placeholder="First Name"
          required
          ref={firstNameRef}
          onChange={handleChange}
        />
        <br />
        <Form.Control
          type="text"
          placeholder="Last Name"
          required
          ref={lastNameRef}
          onChange={handleChange}
        />
        <br />
        <Form.Control
          type="text"
          placeholder="Mobile Number"
          required
          ref={mobileNoRef}
          onChange={handleChange}
        />
        <br />
        <Form.Control
          type="email"
          placeholder="Email"
          required
          ref={emailRef}
          onChange={handleChange}
        />
        <br />
        <Form.Control
          type="password"
          placeholder="Password"
          required
          ref={password1Ref}
          onChange={handleChange}
        />
        <br />
        <Form.Control
          type="password"
          placeholder="Verify Password"
          required
          ref={password2Ref}
          onChange={handleChange}
        />
        <br />
        <Form.Check
          type="checkbox"
          label="I want to receive notifications regarding promotions and exclusive offers via SMS."
          className="float-left"
        />
        <button className="login-btn-submit" disabled={isActive}>
          Register
        </button>
        <br />
        <p className="pt-2 registration-policy">
          By proceeding to register, I acknowledge that I have read and
          consented to KBuild's{" "}
          <a className="registration-policy-link" href="#">
            Terms of Use
          </a>{" "}
          and{" "}
          <a className="registration-policy-link" href="#">
            Privacy Policy
          </a>{" "}
          , which sets out how KBuild collects, uses and discloses my personal
          data, and the rights that I have under applicable law.
        </p>
      </Form.Group>
    </Form>
  );
};

const Tabs = ({ config }) => {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className="d-block justify-content-center login-reg-tab">
      <div className="login-reg-tab-headers">
        {config.map((entry, index) => (
          <div
            className={`login-reg-tab-header  ${
              activeTab === index ? "active" : ""
            } `}
            onClick={() => setActiveTab(index)}
            key={index}
          >
            {entry.header}
          </div>
        ))}
      </div>
      <div className="login-reg-tab-body">{config[activeTab].component}</div>
    </div>
  );
};

export default function LoginRegister() {
  return (
    <Container className="col-md-5 col-xs-10">
      <Tabs
        config={[
          { header: "Login", component: <Login /> },
          { header: "Register", component: <Register /> },
        ]}
      />
    </Container>
  );
}
