// import { useEffect, useState } from "react";
import React from "react";
import { Carousel, Container } from "react-bootstrap";
import "../css/home.css";
export default function Home() {
  return (
    <Container className="col-10">
      <Carousel className="mt-5 ">
        <Carousel.Item interval={2000}>
          <img
            className="d-block home-carousel-img"
            src="https://res.cloudinary.com/dste9shez/image/upload/v1677488273/Home_Page/republic_cement_kcfabb.jpg"
            alt="First slide"
          />
          {/* <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption> */}
        </Carousel.Item>
        <Carousel.Item interval={500}>
          <img
            className="d-block home-carousel-img"
            src="https://res.cloudinary.com/dste9shez/image/upload/v1677488273/Home_Page/rebar_iikt1y.jpg"
            alt="Second slide"
          />
          {/* <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption> */}
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block home-carousel-img"
            src="https://res.cloudinary.com/dste9shez/image/upload/v1677488274/Home_Page/bostik_s9v0zo.jpg"
            alt="Third slide"
          />
          {/* <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption> */}
        </Carousel.Item>
      </Carousel>
    </Container>
  );
}
