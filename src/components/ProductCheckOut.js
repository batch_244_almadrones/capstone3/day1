import { Link } from "react-router-dom";
import { Card, Button } from "react-bootstrap";

export default function CourseCard({ products }) {
  const { image, name, description, price } = products.product;
  const productQty = products.productQty;
  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>
        <Button as={Link} variant="primary" to={`/productView/${_id}`}>
          Details
        </Button>
      </Card.Body>
    </Card>
  );
}
