import { useContext, useState } from "react";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import "../css/navbar.css";
import logo from "./logoHardware.PNG";
import { BiUserCircle } from "react-icons/bi";
export default function AppNavBar() {
  const { user } = useContext(UserContext);
  const [expanded, setExpanded] = useState(false);
  console.log(user);
  const navDropdownTitle = (
    <div className="d-inline-flex">
      <BiUserCircle className="navbar-user-icon" />
      <div>
        <p className="m-0 p-0 navbar-log-reg-link">{user.firstName}</p>
        <p className="m-0 p-0 navbar-log-reg-link">{user.lastName}</p>
      </div>
    </div>
  );

  return (
    <Navbar expanded={expanded} expand="lg" className="sticky-top navbar">
      <Container>
        <Nav className="me-auto">
          <Navbar.Brand as={Link} to="/">
            <img alt="" src={logo} className="img-responsive company-logo" />{" "}
            <span className="company-name">KBuild</span>
          </Navbar.Brand>
        </Nav>

        <Navbar.Toggle
          onClick={() => setExpanded(expanded ? false : true)}
          aria-controls="responsive-navbar-nav"
        />
        <Navbar.Collapse id="responsive-navbar-nav ">
          <Nav className="ms-auto d-inline-flex align-items-center">
            <Nav.Link
              as={NavLink}
              to="/shop"
              onClick={() => setExpanded(false)}
            >
              Shop
            </Nav.Link>

            <Nav.Link
              as={NavLink}
              to="/myProduct"
              state={{ section: "cart" }}
              onClick={() => setExpanded(false)}
            >
              Cart
            </Nav.Link>

            <Nav.Link
              as={NavLink}
              to="/myProduct"
              state={{ section: "wishlist" }}
              end
              onClick={() => setExpanded(false)}
            >
              Wishlist
            </Nav.Link>

            {user._id == null ? (
              <Nav.Link as={NavLink} to="/login" className="d-inline-flex">
                <BiUserCircle className="navbar-user-icon" />
                <div>
                  <p className="m-0 p-0 navbar-log-reg-link">Log in</p>
                  <p className="m-0 p-0 navbar-log-reg-link">Register</p>
                </div>
              </Nav.Link>
            ) : (
              <NavDropdown title={navDropdownTitle} id="basic-nav-dropdown">
                <NavDropdown.Item hidden={user.isAdmin ? false : true}>
                  <Nav.Link
                    as={NavLink}
                    to="/dashboard"
                    onClick={() => setExpanded(false)}
                  >
                    Dashboard
                  </Nav.Link>
                </NavDropdown.Item>
                <NavDropdown.Item hidden={user.isAdmin ? true : false}>
                  My Account
                </NavDropdown.Item>
                <NavDropdown.Item hidden={user.isAdmin ? true : false}>
                  <Nav.Link
                    as={NavLink}
                    to="/myProduct"
                    state={{ section: "orders" }}
                    onClick={() => setExpanded(false)}
                  >
                    My Orders
                  </Nav.Link>
                </NavDropdown.Item>
                <NavDropdown.Divider />

                <NavDropdown.Item>
                  <Nav.Link
                    as={NavLink}
                    to="/logout"
                    onClick={() => setExpanded(false)}
                  >
                    Logout
                  </Nav.Link>
                </NavDropdown.Item>
              </NavDropdown>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
